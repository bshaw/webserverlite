'use-strict'

require("./plugin.js");
module.exports = class Plugins {
	static get version() {
		return "0.0.2.872019";
	} 
	constructor(invoker) {
		this.invoker = invoker;
		this.plugs = [];
		var path = FS.realpathSync("./plugins");
		var files = FS.readdirSync(path);
		files.forEach((e) => {
			var plugin = require(FS.realpathSync("./plugins/" + e));
			var plug = new plugin(invoker);
			if (plug.readyState) {
				this.plugs.push(plug);
				log("* A wild " + plug.name + " appeared!");
			}
		});
		files.sort(function(a, b) {
			if (a.priority === undefined || b.priority === undefined) return 1;
			if (a.priority<b.priority) {
				return 1;
			}
			if (a.priority>b.priority) {
				return 0;
			}
			return 0;
		});
	}
	get plugins() {
		return this.plugs;
	}
	getPluginByName(name) {
		for(var i = 0; i < this.plugs.length; i++) {
			if (this.plugs[i].name.toLowerCase() === name.toLowerCase()) {
				return this.plugs[i];
			}
		}
		return null;
	}
	reload(str) {
		var plugin = require(FS.realpathSync("./plugins/" + str));
		var plug = new plugin(this.invoker);
		if (plug.readyState) {
			this.plugs.push(plug);
			log("\n* A wild " + plug.name + " appeared!");
		}
	}
	remove(name) {
		for(var i = 0; i < this.plugs.length; i++) {
			if (this.plugs[i].name.toLowerCase() === name.toLowerCase()) {
				this.plugs[i].dispose();
				log("\n* Removed plugin: " + name);
				this.plugs.splice(1, i);
				return;
			}
		}
	}
}
