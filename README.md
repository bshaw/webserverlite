**How to use:**   
Extract anywhere but the steam folder and run the exe.  
Alternatively from the commandline you may use:  
`node ./main.js -config="path/to/my/config.json"`  

Using a pkg build:  
Windows: `./WebServerLite-win.exe -config="path/to/my/config.json"`  
Linux: `chmod +x ./WebServerLite-linux; ./WebServerLite-Linux -config="path/to/my/config.json"`  
MacOS: `???`

WebServerLite loads `apirequest.js`, `preprocessor.js` and scripts found in `./plugins/` from disk and requires scripts be located in the same working path as the binary or `main.js` script. 

Here is a typical config.json file, note the comments should be removed otherwise config is invalid:
```javascript
{
	"ssl":
	{
		"enabled": false,
		"port": 4443,
		"paths": {
			"cert": "/acme/cert.pem",
			"key": "/acme/privkey.pem"
		}
	},
	"port": 8080,
	"versioner": false, // enables hotpatching on launch
	"session": false, // enables session
	"logging": false, // write error/warnings to file
	"fs": {
		"root": "bin\\www\\root\\", // path to www root
		"log": "bin\\www\\logs\\", // where to save error/warnings
		"defaultIndex": "/index.html" // default page to serve when nothing else is requested
	},
	"php": {
		"path": "/path/to/php/" // path to where php binaries are located
	}
}
```
(sorry for the mix n match path/stuff/)

**Features**  
 **August 2019**  
 *added support for HTTPS*  
 *added support the php parser*⁵  

 **untracked features**  
 *serve files over http 1.1*  
 *message/error logging*¹  
 *session mode designed to hide root folder for public testing*²  
 *directory traversal protection*  
 *preprocessor and api is open and easily modified per user*³  
 *plugin system that is poorly implemented and just bad*⁴  
 *hot patching for css and javascript files that prolly works*  


**Notes & Bugs**  
 *Chrome and Firefox do not seem to set a referer header when using the CSS url function. If session mode is being used this will result in the server rejecting any resource requests unless the CSS style sheet is located in the same, relative or top level directory as the resource.*  
 *GET requests are only implemented in `PhpParser.js`*  
 *No support for POST yet, due to callback hell*  
 *doesn't have a proper cgi implementation*  

¹ message logging options are not as verbose as they could be  
² during session mode all requests, including referer, must include the session key  
³ preprocessor and api are experimental, suck and probably shouldn't be called the way they do  
⁴ also shouldnt probably be called a plugin system  
⁵ does not have cgi fully implemented yet, but maybe some day though. untested, probably super insecure because php-cgi and because untested. wouldn't recommend for public sessions 
