'use-strict';

module.exports = class Consoler extends Plugin {
	get version() {
		return "0.0.1.872019";
	}
	get name() {
		return "Consoler";
	}

	constructor(invoker) {
		super(invoker);

		this.invoker = invoker;
		const readline = require("readline");

		readline.emitKeypressEvents(process.stdin);
		process.stdin.setRawMode(true);

		var input = "";
		var exit = 0;

		process.stdin.on('keypress', (str, key) => {

			if (key.sequence === '\n' || key.sequence === '\r') {
				this.process(input);
				input = "";
			}
			else if (key.ctrl == true && key.name === 'c') {
				if (exit == 1) {
					process.exit();
				}
				console.log("(To exit, press ^c again)");;
				exit = 1;
				return;
			} 
			else if (key.sequence === '\b') {
				input = input.substring(0, input.length - 1);
			}
			else {
				input += key.sequence;
				exit = 0;
			}

			process.stdout.cursorTo(0, process.stdout.rows - 1);
			process.stdout.clearLine(0);
			process.stdout.write("> " + input);

		});

		this.readyState = true;
	}

	process(msg) {

		var outmsg = msg;
		var input = msg.split(" ");
		switch(input[0].toLowerCase()) {
			case "help": {
				outmsg = "------------------------\n";
				outmsg += " help\t\t\t- show this list\n";
				outmsg += " versioner-upgrades\t- print number of current websocket connections\n";
				outmsg += " versioner {0,1}\t- toggle versioner plugin. if toggled off, you must relaunch the webserver app to use again\n";
				outmsg += "--------------------------\n";
				break;
			}
			case "versioner-upgrades": {
				var ver = this.invoker.plugins.getPluginByName("Versioner");
				if (ver != null) 
					outmsg = "Active upgrades: " + ver.activeUpgrades;
				break;
			}
			case "versioner": {
				if (input[1] == 1) {
					var ver = this.invoker.plugins.getPluginByName("Versioner");
					if (ver != null) {
						outmsg = "versioner is already active";
					}
					else {
						config.versioner = true;
						this.invoker.plugins.reload("versioner.js");
					}
				}
				else {
					config.versioner = false;
					this.invoker.plugins.remove("Versioner");
				}
				break;
			}
			default: {
				outmsg = "Unknown command: " + input[0] + ". Use 'help' for commands.";
				break;
			}
		}

		process.stdout.cursorTo(0, process.stdout.rows);
		process.stdout.clearLine(0);
		console.log("~ " + outmsg);
	}

}