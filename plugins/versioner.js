'use-strict';

module.exports = class Versioner extends Plugin {
	get version() {
		return "0.0.3.872019";
	}
	get name() {
		return "Versioner";
	}
	getFiles(dir) {
		// todo: this could probably be improved a lot
		var fallback = true;
		if (fallback) {
			var files = [];
			var tfiles = FS.readdirSync(dir);
			var _dir = "";
			for(var i = 0; i < tfiles.length; i++) {
				var stats = FS.statSync(dir + PATH.sep + tfiles[i]);
				if (stats.isDirectory()) {
					var f = this.getFiles(dir + PATH.sep + tfiles[i]);
					for(var x = 0; x < f.length; x++) {
						files.push({file: PATH.sep + tfiles[i] + PATH.sep + f[x].file, lastModTime: f[x].lastModTime});
					}
				} else {
					files.push({file: PATH.sep + tfiles[i], lastModTime: Math.floor(stats.mtimeMs)});
				}
			}
			return files;
		}
	}
	constructor(invoker) {
		super(invoker);

		if (!invoker.getSettings().versioner) {
			return;
		}

		this.activeUpgrades = 0;

		invoker.getServer().on('upgrade', (a, b, c) => { this.connectionUpgradeCallback(a, b, c); });
		this.intervals = [];
		//this.root = invoker.getSettings().fs.root;

		this.sessionid = (invoker.getSettings().session ? invoker.getSettings().sessionid : "");
		this.sockets = [];

		this.events = new EVENT();

		this.files = [];
		this.fileWatch = setInterval(() => {
			var files = this.getFiles(config.fs.root);
			if (this.readyState == false) { return; }
			for(var i = 0; i < files.length; i++) {
				if (this.files[files[i].file] === undefined) {
					this.files[files[i].file] = files[i].lastModTime;
				}
				if (this.files[files[i].file] !== files[i].lastModTime) {
					this.files[files[i].file] = files[i].lastModTime;
					this.events.emit("vchange", files[i].file);
				}
			}
		}, 1000);
		this.readyState = true;
	}
	wsMessageToBytes(msg/*, hb=false*/) {
		var tmsg = {fin: true, rsv1: false, rsv2: false, rsv3: false, opcode: 0x1/*hb ? 0x9 : 0x1*/}
		
		var bytes = [];
		bytes[0] = ((((((((tmsg.fin << 1) | tmsg.rsv1) << 1) | tmsg.rsv2) << 1) | tmsg.rsv3) << 4) | tmsg.opcode);
		
		var o2 = 2;
		if (msg.length < 126) {
			bytes[1] = msg.length;
		}
		else if (msg.length <= 65535) {
			//console.log("less than 65535")
			bytes[1] = 126;
			bytes[2] = (msg.length >>> 8) & 0xff;
			bytes[3] = msg.length & 0xff;
			o2 = 4;
		}
		else if (msg.length > 65535) {
			//console.log("more than 65535");
			bytes[1] = 127;
			bytes[2] = 0;// reeee 
			bytes[3] = 0;// no 64 bits
			bytes[4] = 0;// in jabbascribblandia
			bytes[5] = 0;// reeee
			bytes[6] = (msg.length >>> 24) 	& 0xff;
			bytes[7] = (msg.length >>> 16)	& 0xff;
			bytes[8] = (msg.length >>> 8)	& 0xff;
			bytes[9] = msg.length 			& 0xff;
			o2 = 10;
		}
		
		for(var i = 0; i < msg.length; i++) {
			bytes[o2+i] = msg[i].charCodeAt(0);
		}
		return bytes;
	}
	connectionUpgradeCallback(request, socket, headers) { // todo: specific to versioner plugin thing
		if (this.readyState != true) {
			socket.end();
			socket.destroy();
			console.log("asdasd???")
			return;
		}
		if (this.sessionid !== "") {
			if (request.url.replace("/", '') !== this.sessionid) {
				log("** Warning: bad session id from: " + request.connection.remoteAddress);
				socket.end();
				socket.destroy();
				return;
			}
		}

		// todo: perhaps make upgrade only possible for localhost?
		var self = this;
		if (request.headers.upgrade.toLowerCase() === "websocket") { // todo: tested only with chrome
			log("* Notice: websocket upgrade request from: " + request.connection.remoteAddress);
			this.activeUpgrades++;

			socket.on('error', function(a, b, c) {
				console.log(a, b, c);
				clean();
				return;
			});

			socket.on('data', function(a, b, c) {
				if (self.readyState != true) {
					clean();
					return;
				}
				// todo: websocket control frames are not fully implemented
				// fragmentation is not supported

				var data = {buffer: a};
				
				var offset = 0;
				data.fin  		= ((a[offset] & 0x80) != 0);
				data.rsv1 		= ((a[offset] & 0x40) != 0);
				data.rsv2 		= ((a[offset] & 0x20) != 0);
				data.rsv3 		= ((a[offset] & 0x10) != 0);
				data.opcode 	= ( a[offset] & 0x0f);

				offset++;

				data.mask		= ( a[offset] & 0x80) != 0;
				data.len 		= ( a[offset] & 0x7f);

				offset++;

				if (data.len == 126) {
					data.len2		= ( a[offset++] << 8) | a[offset++];
				}
				else if (data.len == 127) {
					data.len2		= ((((((((((((((a[offset++]  << 8) | a[offset++]) << 8) | 
													a[offset++]) << 8) | a[offset++]) << 8) | 
													a[offset++]) << 8) | a[offset++]) << 8) |
													a[offset++]) << 8) | a[offset++]); 
				}
				data.maskingkey = [];
				if (data.mask == true) {					
					data.maskingkey[0] = a[offset++]
					data.maskingkey[1] = a[offset++]
					data.maskingkey[2] = a[offset++]
					data.maskingkey[3] = a[offset++]
				}

				data.payload = [];
				for(var i = 0; i < (data.len2 || data.len); i++) {
					data.payload.push(String.fromCharCode(a[offset++] ^ data.maskingkey[i % 4]));
				}
				data.maskingkey16 = (data.maskingkey).toString(16);
				data.payload.toString = () => { return data.payload.join(''); };

				//var mbuff = Buffer.from(self.wsMessageToBytes("Hello world!\n"));
				//console.log(mbuff);


				//socket.write(mbuff);//Buffer.from(mbytes));
				
				// todo: 
				if (data.opcode !== 1) { // todo: for now, only process text me
					log("** Warning: websocket control frame opcode is not 1: " + data.opcode);
				}
				if (data.opcode == 8) { // client has disconnect
					clean();
				}
				// todo: 
				if (data.len >= 1000 || data.mask == false) {	
					log("** Warning: bad len and/or mask");
				}

			});

			var guid = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
			var clientKey = request.headers['Sec-WebSocket-Key'] || request.headers['sec-websocket-key'];
			var hash = CRYPTO.createHash('sha1');
			hash.update(clientKey+guid);
			var replyKey = hash.digest().toString('base64');

			socket.write(	'HTTP/1.1 101 Switching Protocols\r\n' +
							'Upgrade: WebSocket\r\n' +
							'Connection: Upgrade\r\n' +
							'Sec-WebSocket-Accept: ' + replyKey + "\r\n" +
							'\r\n');
			socket.setKeepAlive(true, 1000);
			socket.setTimeout(0);

			function clean() {
				socket.end();
				socket.destroy();
				self.events.removeListener('vchange', vchangeCallback);
				self.activeUpgrades--;
			}
			function vchangeCallback(file) {
				socket.write(Buffer.from(self.wsMessageToBytes( JSON.stringify({ f: file } ))));
			}
			this.events.addListener('vchange', vchangeCallback);
		};
		
	}

	dispose() {
		clearInterval(this.fileWatch);
		this.readyState = false;
	}

	parse(text, headers) {
		if (headers["referer"] !== undefined/* || status !== 200*/) {
			return text; 
		}
		function injected_versioner() {
			function versioner_upgrade() { 
				var socket = this.socket = new WebSocket("ws://" + location.host + "/<Psessionid>"); // preprocessor.js replaces <Psessionid> with sessionid or ""

				socket.addEventListener('open', function(e) {
					console.log("Versioner: ws open: " + e.data);
				});

				socket.addEventListener('message', function(e) {
					var msg = JSON.parse(e.data);
					//console.log(msg);
					if (msg.f !== undefined || msg.f !== null) {
						var split = msg.f.split(".");
						var type = split[split.length - 1];
						//console.log(type);
						if (type === "js") {
							versioner_myeval(msg.f);
						}
						else {
							versioner_load(msg.f, type);
						}
					} 

				});

				socket.addEventListener('error', function(e) {
					console.log("Versioner: ws event: error: " + e.data);
				});

				socket.addEventListener('close', function(e) {
					console.log("Versioner: ws event: close: " + e.data);
				});
			}

			function versioner_load(url, type) {
				var req = new XMLHttpRequest();
				req.open('GET', url + "?v=" + (new Date()).getTime());
				req.onreadystatechange = function(r) {
					if (req.readyState === 4 && req.status === 200) {

						if (type === 'css') {
							versioner_myeval2(req.responseText);
						}
						console.log("Versioner: reloaded sauce: " + url);
					}
				}
				req.send();
			}
			function versioner_myeval(url) {
				var node = document.getElementById("versioner_loader");
				if (node !== null) {
					document.body.removeChild(node);
				};
				var node = document.createElement('script');
				node.type = "application/javascript";
				node.setAttribute('id', 'versioner_loader');
				node.src = url + "?v=" + (new Date()).getTime();

				document.body.appendChild(node);
				document.body.removeChild(node);

				console.log("Versioner: reloaded sauce: " + url);

			}
			function versioner_myeval2(text) {

				var node = document.getElementById("versioner_loader2");
				if (node !== null) {
					document.body.removeChild(node);
				}
				node = document.createElement('style');
				node.setAttribute('id', 'versioner_loader2');
				node.type = "text/css";
				
				node.appendChild(document.createTextNode(text));
				document.body.appendChild(node);
				//document.body.removeChild(node);

			}
			versioner_upgrade();
			console.log("hello world from versioner");
			
		}

		var txt = text.replace(/(<head>)/, "<head><script type=\"application/javascript\">(" + injected_versioner.toString() + ")();</script>");;
		return txt;
	}
}