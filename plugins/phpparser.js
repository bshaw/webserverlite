'use-strict';

const util = require("util");

module.exports = class PhpParser extends Plugin {
	// todo: webserver.js 
	get version() {
		return "0.0.1.872019";
	}
	get name() {
		return "PhpParser";
	}

	constructor(invoker) {
		super(invoker);
		this.priority = 0;

		this.phpPath = (config.php !== undefined ? config.php.path : "C:\\php\\");
		this.phpCommand ="php-cgi.exe";

		
		this.readyState = true;
	}

	parse(text, ignore, file, request){

		var type = PATH.extname(file);

		var url = request.url;
		if (request.url.includes("?")) {
			url = url.split("?")[1];
		}
		var queryStrings = new QUERY.parse(url);//QUERY.parse.
		var args = [ '-f', file, '-c', this.phpPath ];
		for(var key in queryStrings) {
			var str = key + "=" + queryStrings[key];
			args.push(str);
		}
		//var queryStrings = QUERY.parse(request.);
	
		var otext = text;
		if (type !== ".php") { return otext; }; // do nothing i guess, idk
		var exe = require('child_process').spawnSync;
		try {
			//var args = ['-i', '-c', "E:/Programs/php/"];
			var text = exe(this.phpPath + this.phpCommand, args);
			console.log("* Plugin Notice: parsed php file:", file );
			return text.stdout.toString();
		}
		catch (e) {
			console.log("error parsing:", e);
		}
		return otext;
	}


}