'use-strict';

module.exports = class Preprocessor {
	static get version() {
		return "0.0.4.872019";
	}
	constructor(buffer, headers, settings, file, request) {
		this.buffer = buffer;
		this.headers = headers;
		//console.log(headers);
		this.sessionid = settings.sessionid;
		this.file = file;

		this.request = request;
	}
	parse(plugs) {
		// todo: preprocessing
		var self = this;
		var tmp = this.buffer.toString();
		//new PhpParser(file);
		plugs.plugins.forEach((e) => {
			if (typeof e.parse === "function")
				tmp = e.parse(tmp, self.headers, self.file, this.request);

		});
		return Buffer.from(tmp.replace('\<Pversion\>', Preprocessor.version).replace('\<Psessionid\>', this.sessionid).replace('\<Wversion\>', Server.version));
	}
};