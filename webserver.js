'use strict';

module.exports = class WebServer {
	static get version() {
		return "0.0.6.892019";
	}
	constructor() { 
		//config = Object.assign({}, config);

		// tmp
		var self = this;
		var date = new Date();

		this.clocks =  {start: new Date(), timestamp: date.getMonth() + '' + date.getDay() + '' + date.getFullYear() + '-' + date.getTime()};


		this.stats = {logs: 0, requests: 0}; // todo: debugging, basically unused

		log("# WebServer has initiated... using packages:\n\tWebServer.js\t\tVersion: " + WebServer.version);
		log("\tApiRequest.js\t\tVersion: " + API.version + "\n\tPreprocessor.js\t\tVersion: " + PREP.version + "\n\tPlugins.js\t\tVersion: " + PLUG.version + "\n");

		log("# RootDir="+FS.realpathSync(config.fs.root)+"\n# LogDir="+FS.realpathSync(config.fs.log) + "\n");
		if (config.fs.root == FS.realpathSync("./") || config.fs.root === (FS.realpathSync("./")) || config.fs.root === "./" || config.fs.root === "/") {
			log("** Warning: root is set to ./ !!!\n");
		}
		else if (config.fs.root === undefined || config.fs.root === null || config.fs.root === "") {
			log("*** Error: root is not set!", 1);
			process.exit();

		}
	}
	getSettings() {
		return config;
	}
	getServer() {
		return this.server;
	}

	// server init
	start() {
		if (config.session) { // randomize the session key here
			config.sessionid = Buffer.from((()=>{
				var m = [];
				for(var i = 0; i < 5; i++) {
					m.push(10+Math.floor(Math.random()*89));
				}
				return m.join('');
			})().toString()).toString('base64').replace(/=/g, "");
			log("* Generated Session ID: " + config.sessionid, 1);
		} else {
			log("* Notice: session is not enabled");
		}
		
		this.serverSSL = null;
		// create servers here
		var listenLog = [];
		var listenLog2 = [];
		listenLog2.push("* Session links:");
		if (config.ssl.enabled) {
			var sslopt = {
				key: FS.readFileSync(config.ssl.paths.key),
				cert: FS.readFileSync(config.ssl.paths.cert)
			}
			this.serverSSL = HTTPS.createServer(sslopt, (a, b) => { this.serve(a, b); } );
			if (this.serverSSL.listen(config.ssl.port)) {
				listenLog.push("* Notice: HTTPS listen port is " + config.ssl.port);
				listenLog2.push("\thttps://localhost:"+config.ssl.port+"/"+(config.sessionid.length > 1 ? config.sessionid + "/" : ""));
			}
			else {// todo: never gets here if port is already in use
				log("*** Error: failed to start SSL webserver on port " + config.ssl.port + " ***\n", 1);
				process.exit();
			}
		}

		this.server = HTTP.createServer( (a, b) => { this.serve(a, b); } );
		if (this.server.listen(config.port)) {
			listenLog.push("* Notice: HTTP listen port is " + config.port);
			listenLog2.push("\thttp://localhost:"+config.port+"/"+(config.sessionid.length > 1 ? config.sessionid + "/" : ""));
		}
		else { // todo: never gets here if port is already in use
			log("*** Error: failed to start webserver on port " + config.port + " ***\n", 1);
			process.exit();
		}


		log(listenLog.join('\n'), 1);
		log(listenLog2.join('\n'));
		
		this.plugins = new PLUG(this);

	}

	// http request callback
	// broken in firefox on localhost until about:config has been modified
	// todo: this is a mess
	serve(request, response) {
		if (request.method.toLowerCase() === 'get') {
			this.getRequest(request, response);
			return;
		}
		else if (request.method.toLowerCase() === 'head') {
			this.getRequest(request, response);
			return;
		}
		console.log("*** Warning: unimplemented request received ");
		console.log(STATUS.s505);
		this.endResponse(response, STATUS.s501);

	}
	// todo: in the next refactor for cgi these request functions can definitely use work
	headRequest(request, response) {
		if (request.headers['upgrade'] !== undefined) { return; }; // upgrades are treated as gibberish requests if not handled here

		var connAddr = request.connection.remoteAddress;
		var subdomain = request.headers.host.split(".")[0] || "";
		var session = this.extractSession(request);
		var fullFilePath = URL.parse(request.url).pathname;

		var f = fullFilePath.split("/");
		var fileName = f[f.length -1];
		var dirName = PATH.dirname(fullFilePath);
		var resource = '<!DOCTYPE html><html><head></head><body>Bad request</body></html>';

		if (config.session) { // sessions only purpose is to hide root behind a randomized key
			if (session !== config.sessionid) {
				log({msg: "** Warning: bad session id", ip: connAddr}, 1);
				this.endResponse(response, STATUS.s403);//{status: 418, type: "text/html", resource: resource});
				return;
			} else {
				fullFilePath = URL.parse(request.url.replace(new RegExp(config.sessionid + "/", 'g'), "")).pathname; // remove session id
				dirName = PATH.dirname(fullFilePath);
			}
		}

		var notResourceRequest = (subdomain === "api" ? true : false);
		if (notResourceRequest) { // todo: no real non-file support, only experimental
			var key = fullFilePath.split("/")[1];
			var json = (new API(request, key)).toString();
			this.endResponse(response, {status: 200, type: "application/json", cors: true, resource: json});
			log({msg: "** Warning: ApiRequest from ip: " + connAddr + ", " + key, key: key}, 1);
		}
		else {
			var _fileName = config.fs.root + fullFilePath;
			if (fullFilePath === "/") { // for cases such as http://localhost/
				_fileName = config.fs.root + "/index.html";
			}
			if (isInRoot(dirName, config.fs.root)) {// check for directory traversal
				var f = FS.readFile(_fileName, (err, res) => {
					var type = getContentType(_fileName);
					var status = 200;
					if (err) {
						log({msg: "** Warning: file not found: " + fileName});
						this.endResponse(response, STATUS.s404);
					}
					else {
						resource = res;
						if (type == "text/html") { // todo: preprocessor stuff
							resource = (new PREP(res, request.headers, this.getSettings(), _fileName, request)).parse(this.plugins);
						}
						log({msg: "* Served: " + fileName + ", to: " + connAddr }, 0);
						this.endResponse(response, {status: status, type: type, resource: "", }, true);
					}
				});
			}
			else {
				log({msg: "** Warning:" + connAddr + " requested a resource outside of root:\n " + fullFilePath}, 1);
				this.endResponse(response, STATUS.s400);//{status: 400, type: "text/html", resource: resource});
				return;
			}
		}
	}
	getRequest(request, response) {

		if (request.headers['upgrade'] !== undefined) { return; }; // upgrades are treated as gibberish requests if not handled here

		var connAddr = request.connection.remoteAddress;
		var subdomain = request.headers.host.split(".")[0] || "";
		var session = this.extractSession(request);
		var fullFilePath = URL.parse(request.url).pathname;

		var f = fullFilePath.split("/");
		var fileName = f[f.length -1];
		var dirName = PATH.dirname(fullFilePath);
		var resource = '<!DOCTYPE html><html><head></head><body>Bad request</body></html>';

		if (config.session) { // sessions only purpose is to hide root behind a randomized key
			if (session !== config.sessionid) {
				log({msg: "** Warning: bad session id", ip: connAddr}, 1);
				this.endResponse(response, STATUS.s403);//{status: 418, type: "text/html", resource: resource});
				return;
			} else {
				fullFilePath = URL.parse(request.url.replace(new RegExp(config.sessionid + "/", 'g'), "")).pathname; // remove session id
				dirName = PATH.dirname(fullFilePath);
			}
		}

		var notResourceRequest = (subdomain === "api" ? true : false);
		if (notResourceRequest) { // todo: no real non-file support, only experimental
			var key = fullFilePath.split("/")[1];
			var json = (new API(request, key)).toString();
			this.endResponse(response, {status: 200, type: "application/json", cors: true, resource: json});
			log({msg: "** Warning: ApiRequest from ip: " + connAddr + ", " + key, key: key}, 1);
		}
		else {
			var _fileName = config.fs.root + fullFilePath;
			if (fullFilePath === "/" || fullFilePath === "") { // for cases such as http://localhost/
				_fileName = PATH.normalize(config.fs.root + config.fs.defaultIndex);
				console.log("serving root index or whatever:", _fileName);
			}
			if (isInRoot(dirName, config.fs.root)) {// check for directory traversal
				var f = FS.readFile(_fileName, (err, res) => {
					var type = getContentType(_fileName);
					var status = 200;
					if (err) {
						log({msg: "** Warning: file not found: " + fileName});
						this.endResponse(response, STATUS.s404);
					}
					else {
						resource = res;
						if (type == "text/html") { // todo: preprocessor stuff
							resource = (new PREP(res, request.headers, this.getSettings(), _fileName, request)).parse(this.plugins);
						}
						log({msg: "* Served: " + fileName + ", to: " + connAddr }, 0);
						this.endResponse(response, {status: status, type: type, resource: resource});
					}
				});
			}
			else {
				log({msg: "** Warning:" + connAddr + " requested a resource outside of root:\n " + fullFilePath}, 1);
				this.endResponse(response, STATUS.s400);//{status: 400, type: "text/html", resource: resource});
				return;
			}
		}
	}

	// todo: 
	endResponse(response, msg, head) {
		if (head === undefined) head = false;
		var headers = {};
		//if (msg.cors == true) { // todo: are some of these even needed?
			headers['Access-Control-Allow-Origin'] = "*";
			headers['Access-Control-Allow-Headers'] = "X-Requested-With";
			headers['Access-Control-Allow-Methods'] = "GET";
		//}
		headers['Content-Type'] = msg.type;
		headers['Content-Length'] = Buffer.byteLength(msg.resource);
		response.writeHead(msg.status, headers);
		if (!head) response.write(msg.resource);
		response.end();
	}

	// extract the session key from http request
	extractSession(request) {
		var referer = request.headers['referer'];
		if (referer !== undefined) {
			return URL.parse(referer).pathname.split("/")[1] || "";
		}
		else {
			return URL.parse(request.url).pathname.split("/")[1] || "";
		}
	};

};