'use-strict'

global.Plugin = class Plugin {
	get readyState() {
		return this._readyState;
	};
	set readyState(v) {
		this._readyState = v;
	};
	get version() {
		return "0.0.0";
	};
	get name() {
		return "plugin name";
	};
	constructor(invoker) {
		this.readyState = false;
	};
	parse(text) {
		return text;
	};
	dispose() {
		this.readyState = false;
	}
}