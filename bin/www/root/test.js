var inc = 1;
function myload(e) {

	var up = null;
	document.getElementById("ajax-send").addEventListener("click", function(){
		load(document.getElementById("ajax-url").value, "ajax-test");
	});
	document.getElementById("api-send").addEventListener("click", function(){
		load("http://api." + location.host + "/" + document.getElementById("api-url").value, "api-test");
	});
	document.getElementById("websocket-connect").addEventListener("click", function(){
		up = new versioner_upgrade();
	});
	document.getElementById("websocket-disconnect").addEventListener("click", function(){
		up.socket.close();
	});

	document.getElementById("websocket-do").addEventListener("click", function(){
		var msgs = [];
		// 150 bytes
		var i = 0;
		while(i < inc) {
			msgs += "red.rum"
			i++;
		}
		
		var arr = msgs;//new Uint32Array(msgs);
		up.rcv.value += "ws send: " + arr.length + "\n";
		up.socket.send(arr);
		//up.socket.send(msgs);

	});
}

function load(url, elem) {
	var req = new XMLHttpRequest();
	req.open('GET', url + "?v=" + (new Date()).getTime());
	req.onreadystatechange = function(r) {
		if (req.readyState === 4 && req.status === 200) {
			document.getElementById(elem).value = req.responseText;
		}
	}
	req.send();
}



// plugin debug stuff
function versioner_upgrade() {
	var socket = this.socket = new WebSocket("ws://" + location.host);

	socket.addEventListener('open', function(e) {
		console.log("Versioner: ws open: " + e.data);
	});

	socket.addEventListener('message', function(e) {
		var msg = JSON.parse(e.data);
		//console.log(msg);
		if (msg.f !== undefined || msg.f !== null) {
			var split = msg.f.split(".");
			var type = split[split.length - 1];
			console.log(type);
			if (type == "js") {
				versioner_myeval(msg.f);
			}
			else {
				versioner_load(msg.f, type);
			}
		} 

	});

	socket.addEventListener('error', function(e) {
		console.log("Versioner: ws event: error: " + e.data);
	});

	socket.addEventListener('close', function(e) {
		console.log("Versioner: ws event: close: " + e.data);
	});
}
function versioner_load(url, type) {
	var req = new XMLHttpRequest();
	req.open('GET', url + "?v=" + (new Date()).getTime());
	req.onreadystatechange = function(r) {
		if (req.readyState === 4 && req.status === 200) {

			if (type === 'css') {
				versioner_myeval2(req.responseText);
			}
			console.log("Versioner: reloaded sauce: " + url);
		}
	}
	req.send();
}
function versioner_myeval(url) {
	var node = document.getElementById("versioner_loader");
	if (node !== null) {
		document.body.removeChild(node);
	};
	var node = document.createElement('script');
	node.type = "application/javascript";
	node.setAttribute('id', 'versioner_loader');
	node.src = url + "?v=" + (new Date()).getTime();

	document.body.appendChild(node);
	document.body.removeChild(node);

	console.log("Versioner: reloaded sauce: " + url);

}
function versioner_myeval2(text) {

	var node = document.createElement('style');
	node.type = "text/css";
	
	node.appendChild(document.createTextNode(text));
	document.body.appendChild(node);

}


function injected_versioner() {


	function versioner_upgrade() { 
		var socket = this.socket = new WebSocket("ws://" + location.host);

		socket.addEventListener('open', function(e) {
				console.log("Versioner: ws open: " + e.data);
		});

		socket.addEventListener('message', function(e) {
			var msg = JSON.parse(e.data);
			//console.log(msg);
			if (msg.f !== undefined || msg.f !== null) {
				var split = msg.f.split(".");
				var type = split[split.length - 1];
				console.log(type);
				if (type == "js") {
					versioner_myeval(msg.f);
				}
				else {
					versioner_load(msg.f, type);
				}
			} 

		});

		socket.addEventListener('error', function(e) {
			console.log("Versioner: ws event: error: " + e.data);
		});

		socket.addEventListener('close', function(e) {
			console.log("Versioner: ws event: close: " + e.data);
		});
	}

	function versioner_load(url, type) {
		var req = new XMLHttpRequest();
		req.open('GET', url + "?v=" + (new Date()).getTime());
		req.onreadystatechange = function(r) {
			if (req.readyState === 4 && req.status === 200) {

				if (type === 'css') {
					versioner_myeval2(req.responseText);
				}
				console.log("Versioner: reloaded sauce: " + url);
			}
		}
		req.send();
	}
	function versioner_myeval(url) {
		var node = document.getElementById("versioner_loader");
		if (node !== null) {
			document.body.removeChild(node);
		};
		var node = document.createElement('script');
		node.type = "application/javascript";
		node.setAttribute('id', 'versioner_loader');
		node.src = url + "?v=" + (new Date()).getTime();

		document.body.appendChild(node);
		document.body.removeChild(node);

		console.log("Versioner: reloaded sauce: " + url);

	}
	function versioner_myeval2(text) {

		var node = document.createElement('style');
		node.type = "text/css";
		
		node.appendChild(document.createTextNode(text));
		document.body.appendChild(node);

	}
	console.log("hello world: "+ versioner_upgrade());
	
}