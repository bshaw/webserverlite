<!DOCTYPE html>
<html>
<head>
	<title>CGI Implementation testing...</title>
	<script type="application/javascript">
		function loadHead() {
		var req = new XMLHttpRequest();
		req.open('HEAD', "/index.html");
		req.onreadystatechange = function(r) {
			if (req.readyState === 4 && req.status === 200) {
				console.log(req.responseText);
			}
		}
		req.send();
	}
	</script>
</head>
<body>

	<?PHP
		echo "<b>echo</b> \$_GET['test'] => ".$_GET['test'];
	?>

	<form type="" method="POST">
		<label>POST support is not implemented.</label>
		<input name="input" type="text" value="Test Value"/>
		<button type="submit">Submit</button>
	</form>

	<button onclick='loadHead()'>Send HEAD request</button>

</body>
</html>