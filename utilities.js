'use-strict';

// awful awful awful macro for WebServer
global.getContentType = function(url) { // todo: MIME type list should be external or imported for custom types
	var ext = PATH.extname(url);
	switch(ext) { // todo: missing most MIME types
		case '.ico': 
			{ return "image/x-icon"; }
		case '.png':
			{ return 'image/png'; }
		case '.jpeg': case '.jpg':
			{ return 'image/jpeg'; }
		case '.gif':
			{ return 'image/gif'; }
		case '.html': case '.htm':
			{ return 'text/html'; }
		case '.css':
			{ return 'text/css'; }
		case '.json': 
			{return 'application/json'}
		case '.js': 
			{ return 'application/javascript'; }
		case '.php':
			{ return 'text/html'; }
		//case '': 
		//{ return ''; }
		default: 
			{ return 'application/octet-stream'; }
		//default:
			//{ return 'text/plain'; }

	}
};
global.log = (()=>{});
global.escape = function(str) {
    return str.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
};
global.isInRoot = function(url, root) {
	try {
		var realPath = FS.realpathSync(root + url);
		var exp = new RegExp(escape(root), "g");
		if (realPath.match(exp) == null) {
			return false;
		}
		return true;
	}
	catch(e) {
		return false;
	}
}

// macro definitions
global.log = ((a, s) => { // expects object or string as a and s=1 to write to disk
	if (typeof a === "string") {
		var a = {msg: a};
	}
	console.log(a.msg);
	if (config.logging) {
		if (s == 1) {
			a.timestamp = (new Date()).getTime();
			FS.appendFile( config.fs.log + "/" + config.startTime + '.log', JSON.stringify(a) + ',\r\n', {flag: 'a'}, function(e, r) {if (e!==null) { console.log("*** Error: Failed to write log"); console.log(e, r); } });
			//this.stats.logs++;
		}
	}
});