'use-strict';

module.exports = class HttpStatus {
	// client errors
	static get s400() { 
		return {
			status: 400,
			type: "text/html",
			resource: '<!DOCTYPE html><html><head><meta charset="utf-8"></head><body>400 - Bad Request</body></html>'
		}
	};
	static get s403() {
		return {
			status: 403, 
			type: "text/html",
			resource: '<!DOCTYPE html><html><head><meta charset="utf-8"></head><body>403 - Forbidden</body></html>'
		}
	};
	static get s404() { 
		return {
			status: 404, 
			type: "text/html",
			resource: '<!DOCTYPE html><html><head><meta charset="utf-8"></head><body>404 - File Not Found</body></html>'
		}
	};
	
	// server errors
	static get s500() { 
		return {
			status: 500, 
			type: "text/html",
			resource: '<!DOCTYPE html><html><head><meta charset="utf-8"></head><body>500 - Internal Server Error</body></html>'
		}
	};
	static get s501() { 
		return {
			status: 501, 
			type: "text/html",
			resource: '<!DOCTYPE html><html><head><meta charset="utf-8"></head><body>501 - Not Implemented</body></html>'
		}
	};

};

